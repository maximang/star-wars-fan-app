import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ResourcesService } from '../services/resources.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isNavbarCollapsed = true;
  resources: any[];
  resourcesSubscription: Subscription;

  constructor(private authService: AuthService,
              private resourcesService: ResourcesService) { }

  ngOnInit() {
    this.resourcesSubscription = this.resourcesService.resourcesSubject.subscribe(
      (resources: any[]) => {
        this.resources = resources;
      }
    );
    this.resourcesService.emitResourcesSubject();
  }

  ngOnDestroy() {
    this.resourcesSubscription.unsubscribe();
  }
}
