import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  isAuth = false;
  isAuthSubject: Subscription;

  constructor(private router: Router,
              private authService: AuthService) { }

  ngOnInit() {
    this.isAuthSubject = this.authService.isAuthSubject.subscribe(
      (isAuth) => {
        this.isAuth = isAuth;

        if (isAuth) {
          this.router.navigate(['/overview']);
        } else {
          this.router.navigate(['/signin']);
        }
      }
    );
    this.authService.emitIsAuthSubject();
  }

  ngOnDestroy() {
    this.isAuthSubject.unsubscribe();
  }
}
