import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../services/resources.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  constructor(private resourcesService: ResourcesService) { }

  ngOnInit() {
    this.resourcesService.getResources();
  }
}
