import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourcesService } from '../services/resources.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {
  @Input() resource: object;
  @Input() type: string;
  @Input() color: string;

  constructor(public activeModal: NgbActiveModal,
              public resourcesServices: ResourcesService) { }
}
