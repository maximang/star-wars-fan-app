import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailsComponent } from '../details/details.component';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent {
  @Input() resource: object;
  @Input() type: string;
  @Input() color: string;
  @Input() summary: string;

  constructor(private modalService: NgbModal) { }

  openDetails() {
    const modalRef = this.modalService.open(DetailsComponent, { centered: true });
    modalRef.componentInstance.resource = this.resource;
    modalRef.componentInstance.type = this.type;
    modalRef.componentInstance.color = this.color;
  }
}
