import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: object; // TODO: User model
  public isAuth: boolean;
  public isAuthSubject = new Subject<boolean>();
  private localStorageKey = 'user';
  private userList = [{
    firstname: 'Maxime',
    lastname: 'Genilloud',
    email: 'maxgenilloud@gmail.com',
    password: '12345678'
  },
  {
    firstname: 'Julien',
    lastname: 'Nicolas',
    email: 'jnicolas@openwt.com',
    password: 'abcdefgh'
  }];

  constructor() {
    this.isAuth = !!localStorage.getItem(this.localStorageKey);
    this.emitIsAuthSubject();

    if (this.isAuth) {
      this.user = JSON.parse(localStorage.getItem(this.localStorageKey));
    }
  }

  signIn(email: string, password: string) {
    const user = this.userList.find((value) => {
      return value.email === email && value.password === password;
    });

    if (user) {
      this.user = {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email
      };
      this.isAuth = true;
      localStorage.setItem(this.localStorageKey, JSON.stringify(this.user));
      this.emitIsAuthSubject();
    } else {
      // TODO: Error handling
      console.warn('User not found');
    }
  }

  signOut() {
    localStorage.removeItem(this.localStorageKey);
    this.user = null;
    this.isAuth = false;
    this.emitIsAuthSubject();
  }

  emitIsAuthSubject() {
    this.isAuthSubject.next(this.isAuth);
  }
}
