import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import * as Fuse from 'fuse.js';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {
  private apiUrl = 'https://swapi.co/api/';
  private resources: any[] = [];
  public resourcesSubject = new Subject<any[]>();
  private colors: string[] = ['primary', 'success', 'danger', 'warning', 'info', 'dark'];
  private searchKeys: string[] = [];
  private searchValue = '';
  private searchTimeout: any = null;

  constructor(private httpClient: HttpClient) { }

  // TODO: Cache in LocalStorage
  public getResources() {
    this.httpClient
    .get<any>(this.apiUrl)
    .subscribe(
      (result) => {
        const colors = this.colors.slice(0);
        this.resources = [];

        Object.entries(result).forEach(([type, url]) => {
          this.resources.push({ type, color: colors.shift() || 'secondary', active: true, results: [] });
          this.getResource(type, url);
        });
      },
      (error) => {
        // TODO: Error handling
        console.warn(error);
      }
    );
  }

  public getResource(type, url) {
    this.httpClient
    .get<any>(url)
    .subscribe(
      (result) => {
        const { results, next } = result;
        const resource = this.resources.find((value) => value.type === type);

        resource.results = [
          ...resource.results,
          ...results
        ];

        this.emitResourcesSubject();

        if (next) {
          this.getResource(type, next);
        } else {
          const blacklist = ['url', 'homeworld', 'created', 'edited'];
          this.searchKeys = [
            ...this.searchKeys,
            ...Object.keys(result.results[0]).filter(key => !blacklist.includes(key) && typeof result.results[0][key] === 'string')
          ];
        }
      },
      (error) => {
        // TODO: Error handling
        console.warn(error);
      }
    );
  }

  public emitResourcesSubject() {
    this.resourcesSubject.next(this.resources.slice());
  }

  public getSummaryInfo(resource) {
    const blacklist = ['name', 'title', 'url', 'homeworld', 'created', 'edited'];
    return Object.keys(resource)
      .filter(key => !blacklist.includes(key) && typeof resource[key] === 'string')
      .map(key => {
        return {
          key: key.replace(/_/g, ' '),
          value: resource[key]
        };
      });
  }

  public getSummary(resource) {
    let text = '';

    this.getSummaryInfo(resource)
      .forEach(value => text += `${value.key}: ${value.value}, `);

    return text;
  }

  public filter(filter) {
    this.resources.forEach((value) => {
      if (filter === 'choose') {
        value.active = true;
      } else if (value.type === filter) {
        value.active = true;
      } else {
        value.active = false;
      }
    });
  }

  public search(value) {
    clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => this.searchValue = value, 500);
  }

  public display() {
    if (this.searchValue === '') {
      return this.resources;
    }

    const results = [];
    const options = {
      shouldSort: true,
      findAllMatches: true,
      threshold: 0.3,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 2,
      keys: this.searchKeys
    };

    this.resources.forEach((value) => {
      const fuse = new Fuse(value.results, options);
      results.push({ ...value, results: fuse.search(this.searchValue) });
    });

    return results;
  }
}
